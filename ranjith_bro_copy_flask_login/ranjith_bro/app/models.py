from app import db,lm
# from flask_login import LoginManager, UserMixin, login_user, logout_user,current_user
from flask_login import UserMixin
class Details(UserMixin,db.Model):
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(500))
  email = db.Column(db.String(500))
  password=db.Column(db.String(500))
  token=db.Column(db.String(500))
  social_id = db.Column(db.String(500),nullable=True,unique=True)
  nickname = db.Column(db.String(500),nullable=True)

  def __init__(self, username, email, password, token, social_id, nickname):
   self.username = username
   self.email = email
   self.password = password
   self.token = token
   self.social_id = social_id
   self.nickname = nickname
