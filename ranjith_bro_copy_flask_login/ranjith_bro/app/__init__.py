import os
from flask import Flask, redirect, url_for, render_template, flash
# from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, logout_user,current_user

from oauth import OAuthSignIn


#Create an Instance of Flask


app = Flask(__name__)
#Include config from config.py
app.config.from_object('config')
app.secret_key = 'some_secret'


app.config['OAUTH_CREDENTIALS'] = {
    'facebook': {
        'id': '121927755116819',
        'secret': 'd94b4f387a110087be117aea4542ccf5'
    },
    'twitter': {
        'id': 'xJ3HAwNBCr9bwBY3jpV3QQNTg',
        'secret': 'VpzkG2C8c7SN2wEUYrQTMQ2qsDvuIlRsQNH8USWLBjZcVGGCK4'
    }
}


#Create an instance of SQLAclhemy
db = SQLAlchemy(app)
lm = LoginManager(app)
lm.login_view = 'submit'
from app import views, models
