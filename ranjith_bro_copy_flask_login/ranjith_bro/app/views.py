from flask import render_template, request, session, abort,flash, redirect, url_for
from app import app, db, lm
from app.models import Details
import jwt
import bcrypt
import re
import os
from bcrypt import hashpw, gensalt

# from flask_login import LoginManager, UserMixin, login_user, logout_user,current_user
from flask_login import current_user,login_user,logout_user
from oauth import OAuthSignIn


@app.route('/login')
def login_page():
	if not session.get('logged_in'):
		return render_template('login.html')
	else:
		return "Hello Boss! <a href='/logout1'>Logout</a>"

@app.route('/login_validate', methods=['POST'])
def do_admin_login():

	USERNAME = request.form['username']
	PASSWORD = request.form['password']
	PASSWORD = PASSWORD.encode('utf-8')
	retrive_user_details= Details.query.filter_by(username=USERNAME).first()
	if not USERNAME or not PASSWORD:
			flash('Please enter all the fields', 'error')
	elif not retrive_user_details:
		flash('User does not exit')
	else:
		pw=str(retrive_user_details.password)
		if hashpw(PASSWORD, pw) == pw:
			session['logged_in']=True
		else:
			flash('wrong password!')
	return login_page()

   

@app.route("/logout1")
def logout():
	session['logged_in'] = False
	return login_page()


@app.route('/' , methods=['POST', 'GET'])
def submit():
	if request.method == 'POST':
		match=re.match('[\w]+@[\w.]+[\w]',request.form['email'])
		if not request.form['username'] or not request.form['email'] or not request.form['password']:
			flash('Please enter all the fields', 'error')
		
		elif match == None:
			flash('Bad email id')

		else:
			a=request.form['username']
			b=request.form['password']
			b = b.encode('utf-8')
			c=request.form['email']
			
			hashed = hashpw(b, gensalt())
			dic={
			'username':a,
			'email':c
			}
			token=jwt.encode(dic,'secret',algorithm='HS256')
			post=Details( request.form['username'], request.form['email'], hashed,token,None,None)
			db.session.add(post)
			db.session.commit()
			flash('You have successfully signed up ')
	return render_template('submit.html')





@lm.user_loader
def load_user(id):
    return Details.query.get(int(id))


# @app.route('/')
# def index():
#     return render_template('index.html')


@app.route('/logout')
def logoutnew():
    logout_user()
    return redirect(url_for('submit'))


@app.route('/authorize/<provider>')
def oauth_authorize(provider):
    if not current_user.is_anonymous:
        return redirect(url_for('submit'))
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()


@app.route('/callback/<provider>')
def oauth_callback(provider):
    if not current_user.is_anonymous:
        return redirect(url_for('submit'))
    oauth = OAuthSignIn.get_provider(provider)
    username, email, password, token, social_id, nickname = oauth.callback()
    if social_id is None:
        flash('Authentication failed.')
        return redirect(url_for('submit'))
    user = Details.query.filter_by(social_id=social_id).first()
    if not user:
        user = Details(username=username,email=email,password=password,token=token,social_id=social_id, nickname=nickname)
        db.session.add(user)
        db.session.commit()
    login_user(user, True)
    return redirect(url_for('submit'))